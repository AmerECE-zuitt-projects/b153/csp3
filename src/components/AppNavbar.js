import { Container } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import navLogo from "../images/logo-white.png";
import { BiSearchAlt } from "react-icons/bi";
import { FaRegUserCircle } from "react-icons/fa";
import { MdShoppingBag } from "react-icons/md";
import { AiOutlineUser } from "react-icons/ai";
import { MdAlternateEmail } from "react-icons/md";
import { BsPhone } from "react-icons/bs";
import { AiOutlineEdit } from "react-icons/ai";
import { FiLogOut } from "react-icons/fi";
import UserContext from "../UserContext";
import { useContext } from "react";

const AppNavbar = () => {
  const { user, unsetUser } = useContext(UserContext);

  // const navigate = useNavigate();

  const logout = () => {
    unsetUser();
  };

  const rightNav = !user.id ? (
    <>
      <Link className="btn common-btn px-3 me-3 register-btn" to="/register">
        Register
      </Link>
      <Link className="btn common-btn px-3  login-btn" to="/login">
        Login
      </Link>
    </>
  ) : (
    <>
      <div className="nav-bag-btn-wrapper">
        <Link className="nav-bag-btn" to="/bag">
          <MdShoppingBag />
        </Link>
        <span>0</span>
      </div>

      {/* <FaRegUserCircle className="user-profile-btn" /> */}

      <Link
        className="btn common-btn px-3  login-btn"
        onClick={logout}
        to="/login"
      >
        Logout
      </Link>
    </>
  );

  return (
    <nav className="navbar">
      <Container className="nav-container d-flex align-items-center justify-content-between">
        <img src={navLogo} alt="nav logo" className="nav-logo" />
        <div className="nav-items">
          <Link className="nav-link" to="/">
            Home
          </Link>
          <Link className="nav-link" to="/">
            Products
          </Link>
          <Link className="nav-link" to="/">
            Contact
          </Link>
        </div>
        <div className="right-nav ">
          {/* <input type="text" className="search-bar" /> */}
          {rightNav}
        </div>
      </Container>
    </nav>
  );
};

export default AppNavbar;
