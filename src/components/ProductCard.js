import { Col, Card } from "react-bootstrap";
// import test from "../images/test.webp";
import { Link } from "react-router-dom";
import { MdShoppingBag } from "react-icons/md";
import { BsFillInfoCircleFill } from "react-icons/bs";
import { useCart } from "react-use-cart";

const ProductCard = ({ productProp }) => {
  const { _id, imageUrl, title, description, price, category, quantity } =
    productProp;

  const { addItem } = useCart();
  const newItem = {
    id: productProp._id,
    img: productProp.imageUrl,
    title: productProp.title,
    price: productProp.price,
    quantity: productProp.quantity,
  };

  return (
    <Col lg="3" md="6">
      <Card className="product-card border-2">
        <Card.Img variant="top" src={imageUrl} className="product-img" />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>Price: ${price}</Card.Text>
          <Link className="btn details-btn" to={`/products/${_id}`}>
            <BsFillInfoCircleFill className="mb-1" /> Details
          </Link>
        </Card.Body>
        <button className="btn bag-btn" onClick={() => addItem(newItem)}>
          <MdShoppingBag />
        </button>
      </Card>
    </Col>
  );
};

export default ProductCard;
