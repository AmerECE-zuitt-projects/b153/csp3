import { useCart } from "react-use-cart";
import { Row, Col, Container, Table } from "react-bootstrap";
import AppNavbar from "../components/AppNavbar";

const Bag = () => {
  const {
    isEmpty,
    totalUniqueItems,
    items,
    totalItems,
    cartTotal,
    updateItemQuantity,
    removeItem,
    emptyCart,
  } = useCart();

  if (isEmpty) return <h1>Your Bag is Empty</h1>;
  return (
    <>
      <AppNavbar />
      <Container>
        <Row className="justify-content-center">
          <Col>
            <h5>
              Bag {totalUniqueItems} - Total Items {totalItems}
            </h5>
            <Table light hover>
              <tbody>
                {items.map((item) => {
                  return (
                    <tr key={item.id}>
                      <td>
                        <img
                          src={item.img}
                          style={{ height: "6rem" }}
                          alt="img"
                        />
                      </td>
                      <td>{item.title}</td>
                      <td>{item.price}</td>
                      <td>Quantity {item.quantity}</td>
                      <td>
                        <button
                          className="btn btn-info ms-2"
                          onClick={() =>
                            updateItemQuantity(item.id, item.quantity - 1)
                          }
                        >
                          -
                        </button>
                        <button
                          className="btn btn-info ms-2"
                          onClick={() =>
                            updateItemQuantity(item.id, item.quantity + 1)
                          }
                        >
                          +
                        </button>
                        <button
                          className="btn btn-danger ms-2"
                          onClick={() => removeItem(item.id)}
                        >
                          Remove Item
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Bag;
