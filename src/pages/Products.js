import { useState, useEffect, useContext } from "react";
import AppNavbar from "../components/AppNavbar";
import UserView from "../components/UserView";
import { Container, Row } from "react-bootstrap";
import UserContext from "../UserContext";
import AdminView from "../components/AdminView";

const Products = () => {
  const { user } = useContext(UserContext);

  let [productsData, setProductsData] = useState([]);

  const [search, setSearch] = useState("");

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((res) => res.json())
      .then((data) => {
        setProductsData(data);
      });
  };

  useEffect(() => {
    fetchData();
    // console.log(productsData);
  }, []);

  const handleSearch = (e) => {
    e.preventDefault();
    setSearch(e.target.value);
  };
  if (search.length > 0) {
    productsData = productsData.filter((i) => {
      return i.title.toLowerCase().match(search);
    });
  }
  return (
    <>
      <AppNavbar />
      <Container>
        <input
          type="text"
          placeholder="Search..."
          onChange={handleSearch}
          value={search}
        />
        {/* <button>Men</button> */}
        {user.isAdmin === true ? (
          <Row className="mt-1 g-4">
            <AdminView productsProp={productsData} fetchData={fetchData} />
          </Row>
        ) : (
          <Row className="mt-1 g-4">
            <UserView productsProp={productsData} />
          </Row>
        )}
      </Container>
    </>
  );
};

export default Products;
